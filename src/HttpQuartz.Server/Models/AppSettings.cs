﻿using System.Collections.Generic;
using Autowired.Core;
using Microsoft.AspNetCore.Hosting;

namespace HttpQuartz.Server.Models
{
    /// <summary>
    /// 对应appsettings.json
    /// </summary>
    [AppSetting]
    public class AppSettings
    {
        /// <summary>
        /// 网站根目录
        /// </summary>
        public string RootUrl { get; set; }

        /// <summary>
        /// 运行环境
        /// </summary>
        public IWebHostEnvironment Environment { get; set; }

        /// <summary>
        /// quartz配置项
        /// </summary>
        public Dictionary<string, string> Quartz { get; set; }
        public string[] SafeClients { get; set; }

        public ServerChanSection ServerChan { get; set; }

        public bool NoticeEnable()
        {
            return ServerChan != null && !string.IsNullOrWhiteSpace(ServerChan.Key) && ServerChan.Enable;
        }
    }

    /// <summary>
    ///  Server酱微信通知http://sc.ftqq.com/
    /// </summary>
    public class ServerChanSection
    {
        public string Key { get; set; }
        public bool Enable { get; set; }
    }
}